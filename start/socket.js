'use strict'

/*
|--------------------------------------------------------------------------
| Websocket
|--------------------------------------------------------------------------
|
| This file is used to register websocket channels and start the Ws server.
| Learn more about same in the official documentation.
| https://adonisjs.com/docs/websocket
|
| For middleware, do check `wsKernel.js` file.
|
*/

const Ws = use('Ws')
let P1 = false;
let P2 = false;
let nick1 = '';
let nick2 = '';
let Partida = false;

Ws.channel('battle', ({ socket }) => {
  socket.on('message:added', (data) => {
    console.log(data);
        if(data.logeo == true){
            if(P1 != true){
                P1 = true
                nick1 = data.nickname;
                socket.broadcastToAll('message', {Player:1,nickname:nick1})
            }
            else if(P2 != true){
                P2 = true
                nick2 = data.nickname;
                socket.broadcastToAll('message', {Player:2,nickname:nick2})
            }
            else{
                socket.broadcastToAll('message',"Full")
            }
        }
        else if(data.logeo == false){
            if(data.player == 1){
                nick1 = '';
                P1 = false;
                socket.broadcastToAll('message', "OK")
            }
            else if(data.player == 2){
                nick2 = '';
                P2 = false;
                socket.broadcastToAll('message', "OK")
            }
        }
        else if(data == "Recarga"){
            P1 = false
            P2 = false
            nick1 = ''
            nick2 = ''
            Partida = false
        }
        else if(data.ganador){
            console.log(data);
            socket.broadcastToAll('message', data)
        }
        else{
            console.log(data);
            socket.broadcast('message', data)
        }
    })
})
